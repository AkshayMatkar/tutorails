class BookController < ApplicationController
  
  def index
	@book = Book.all
  end

  def new
	@book = Book.new
  end

  def create
	@author = Author.find_by_id(params[:book][:author_id])
	@author.books << Book.create(books_attributes)
	if @author.books
		flash[:notice] = "Saved mate!"
		redirect_to book_index_path
	end
  end

  def edit
	@book = Book.find_by_id(params[:id])
  end

  def update
	@book = Book.find_by_id(params[:id])
	if @book.update_attributes(books_attributes)
		flash[:notice] = "Book details updated"
		redirect_to book_index_path
	end
  end

  def destroy
	@book = Book.find_by_id(params[:id])
	if @book.destroy
		flash[:notice] = "Deleted successfullly"
		redirect_to book_index_path
	end
  end

  def books_attributes
	params.require(:book).permit(:name, :author_id)
  end
  
end
