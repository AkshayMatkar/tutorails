class AuthorController < ApplicationController
  def index
	@author = Author.all
  end

  def new
	@author = Author.new
  end

  def create
	
	@author = Author.new(author_attributes)
	if @author.save
		flash[:notice] = "Author created successfully"
		redirect_to author_index_path		
	else
		flash[:notice] = "Oops! Something went wrong"
		redirect_to author_index_path
	end
	
  end

  def edit
	@author = Author.find_by_id(params[:id])
  end

  def update
	@author = Author.find_by_id(params[:id])
	if @author.update_attributes(author_attributes)
		flash[:notice] = "Updated mate!"
		redirect_to author_index_path
	else
		flash[:notice] = "Something not right"
		redirect_to author_index_path
	end
  end
  
  def show
	
  end
  
  def destroy
	@author = Author.find_by_id(params[:id])
	if @author.destroy
		flash[:notice] = "Deleted mate!"
		redirect_to author_index_path
	else
		flash[:notice] = "Something wrong!"
		redirect_to author_index_path
	end
  end
  
  def author_attributes
	params.require(:author).permit(:author, :id)
  end
  
end
