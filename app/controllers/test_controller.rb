class TestController < ApplicationController
	
	def index
		@test = Test.all
	end
	
	def new
		@test = Test.new
	end

	def create
		@test = Test.new(test_attributes)
		if @test.save
			redirect_to test_index_path
		end
	end
	
	def edit
		@test = Test.find_by_id(params[:id])
	end
	
	def update
		@test = Test.find_by_id(params[:id])
		if @test.update_attributes(test_attributes)
			flash[:notice] = "Very cooooollll!!"
			redirect_to test_index_path
		else
			flash[:notice] = "Something went wrong"
			redirect_to test_index_path
		end
	end
	
	def test_attributes
		params.require(:test).permit(:name)
	end
	
	
end
